package net.gcuisinier;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.opentest4j.AssertionFailedError;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public abstract class AbstractTestBase {


    protected Yaml yaml;

    @BeforeEach
    void setUp() {
        yaml = new Yaml(new SafeConstructor());

    }

    protected <T> T loadYaml(String path) throws IOException {
        String yamlContent = IOUtils.resourceToString(path, StandardCharsets.UTF_8);
        Object yamlObjectExpected = yaml.load(yamlContent);

        return (T) yamlObjectExpected;
    }

    protected void verifyAssertObjectEquals(Object expected, Object actual, String expectedMessage){
        AssertionFailedError thrown = assertThrows(
                AssertionFailedError.class,
                () -> YamlAssert.assertObjectEquals(expected, actual)
                , "Expected assertionFailedError");
        assertEquals(expectedMessage, thrown.getMessage());
    }
}
