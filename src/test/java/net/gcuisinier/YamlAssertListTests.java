package net.gcuisinier;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class YamlAssertListTests extends AbstractTestBase {


    @Test
    void testSimpleEquals() throws IOException {

        Object expected = loadYaml("/list.yaml");
        Object actual = loadYaml("/list.yaml");


        YamlAssert.assertObjectEquals(expected, actual);

    }

    @Test
    void testListExtraEntries() throws IOException {

        Map expected = loadYaml("/list.yaml");
        Map actual = loadYaml("/list.yaml");


        ((List) actual.get("testList")).add(new Date());

        verifyAssertObjectEquals(expected, actual, "List /testList size are different\n" +
                "\tExpected :\t4\n" +
                "\tActual :\t5");

    }

    @Test
    void testListDifferentValues() throws IOException {

        Map expected = loadYaml("/list.yaml");
        Map actual = loadYaml("/list.yaml");


        ((List) actual.get("testList")).set(0, "newVaue");

        verifyAssertObjectEquals(expected, actual, "Path /testList[0] have different value\n" +
                "\tExpected :\tvalue1\n" +
                "\tActual :\tnewVaue");

    }

    @Test
    void testListDifferentType() throws IOException {

        Map expected = loadYaml("/list.yaml");
        Map actual = loadYaml("/list.yaml");


        ((List) actual.get("testList")).set(0, 1);

        verifyAssertObjectEquals(expected, actual, "Path /testList[0] type are different");

    }

    @Test
    void testListDifferentTypeObject() throws IOException {

        Map expected = loadYaml("/list.yaml");
        Map actual = loadYaml("/list.yaml");

        Map innerObject = new HashMap();
        innerObject.put("test", "test");
        innerObject.put("test2", 23);

        ((List) actual.get("testList")).set(0, innerObject);

        verifyAssertObjectEquals(expected, actual, "Path /testList[0] type are different");

    }


}