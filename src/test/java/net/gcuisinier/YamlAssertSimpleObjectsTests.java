package net.gcuisinier;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class YamlAssertSimpleObjectsTests extends AbstractTestBase {




    @Test
    void testSimpleEquals() throws IOException {

        HashMap expected =  loadYaml("/simpleYaml.yaml");
        HashMap actual =  loadYaml("/simpleYaml.yaml");

        // Verify
        YamlAssert.assertObjectEquals(expected, actual);

    }

    @Test
    void testExtraKeys() throws IOException {

        HashMap expected =  loadYaml("/simpleYaml.yaml");
        HashMap actual =  loadYaml("/simpleYaml.yaml");

        // Modify
        actual.put("oneOtherKey", 23);

        // Verify
        verifyAssertObjectEquals(expected, actual, "Map <ROOT> have different size\n" +
                "\tExpected :\t5\n" +
                "\tActual :\t6");
    }

    @Test
    void testMissingKeys() throws IOException {

        HashMap expected =  loadYaml("/simpleYaml.yaml");
        HashMap actual =  loadYaml("/simpleYaml.yaml");

        // Modify
        actual.remove("simpleString");

        // Verify
        verifyAssertObjectEquals(expected, actual, "Map <ROOT> have different size\n" +
                "\tExpected :\t5\n" +
                "\tActual :\t4");
    }

    @Test
    void testKeyDifferentType() throws IOException {

        HashMap expected =  loadYaml("/simpleYaml.yaml");
        HashMap actual =  loadYaml("/simpleYaml.yaml");

        // Modify
        actual.put("simpleString", 42);

        // Verify
        verifyAssertObjectEquals(expected, actual, "Path /simpleString have different type");
    }

    @Test
    void testKeyDifferentsKeys() throws IOException {

        HashMap expected =  loadYaml("/simpleYaml.yaml");
        HashMap actual =  loadYaml("/simpleYaml.yaml");

        // Modify
        actual.put("simpleString2", 42);
        actual.remove("simpleString");

        // Verify
        verifyAssertObjectEquals(expected, actual, "Map <ROOT> have different keys\n" +
                "\tExpected :\tsimpleString,simpleLong,simpleDouble,simpleBoolean,simpleDate\n" +
                "\tActual :\tsimpleLong,simpleDouble,simpleBoolean,simpleDate,simpleString2");

    }

}