package net.gcuisinier;

import org.junit.jupiter.api.Assertions;

import java.util.*;
import java.util.stream.Collectors;

public class YamlAssert {

    public static void assertObjectEquals(Object expected, Object actual){
        assertYamlEqualsInternal("", expected, actual);
    }

    private static void assertYamlEqualsInternal(String path, Object expected, Object actual){

        if(expected == null && actual ==null)
            return;
        if(expected == null && actual != null){
            Assertions.fail(failureMessage("Path %s is not null, and should be null", path));
        }
        if(actual == null && expected != null){
            Assertions.fail(failureMessage("Path %s is null, and should not", path));
        }

        if(actual.getClass() != expected.getClass()){
            Assertions.fail(failureMessage("Path %s type are different", path));
        }

        if(expected instanceof Map){
            compareMap(path, (Map) expected, (Map<?, ?>) actual);
        } else if(isSimpleType(expected)){
            compareSimple(path, expected, actual);
        } else if(expected instanceof List){
            compareList(path, (List) expected, (List) actual);
        }

    }

    private static boolean isSimpleType(Object expected) {
        return expected instanceof String || expected instanceof Long
                || expected instanceof Double || expected instanceof Integer
                || expected instanceof Date;
    }

    private static void compareSimple(String localPath, Object expected, Object actual) {
        if(!expected.equals(actual)){
            Assertions.fail(failureMessage("Path %s have different value", localPath, expected, actual));
        }
    }

    private static void compareMap(String path, Map expected, Map actual) {

        if(expected.size() != actual.size()){
            Assertions.fail(failureMessage("Map %s have different size", path, Integer.toString(expected.size()), Integer.toString(actual.size())));
        }

        HashSet workingSet = new HashSet(expected.keySet());
        workingSet.removeAll(actual.keySet());

        if(workingSet.size() > 0){
            String  expectedKeys = (String) expected.keySet().stream().collect(Collectors.joining(","));
            String actualKeys = (String) actual.keySet().stream().collect(Collectors.joining(","));
            Assertions.fail(failureMessage("Map %s have different keys", path, expectedKeys,actualKeys));
        }

        for(Object key : expected.keySet()){
            String localPath = path + "/" + key;
            Object expectedObject = expected.get(key);
            Object actualValue = actual.get(key);
            if(actualValue == null) {
                Assertions.fail(failureMessage("Path %s should not be null", localPath, null,null));
            } else {
                if(!expectedObject.getClass().equals(actualValue.getClass())){
                    Assertions.fail(failureMessage("Path %s have different type", localPath, null,null));
                }
                if(isSimpleType(expectedObject)){
                    compareSimple(localPath, expectedObject, actualValue);
                } else if (expectedObject instanceof Map){
                    assertYamlEqualsInternal(localPath, expectedObject, actualValue);
                }
                else if (expectedObject instanceof List){
                    List expectedList = (List) expectedObject;
                    List actualList = (List) actualValue;
                    compareList(localPath, expectedList, actualList);
                }
            }
        }
    }

    private static void compareList(String localPath, List expectedList, List actualList) {
        if(expectedList.size() != actualList.size()){
            Assertions.fail(failureMessage("List %s size are different", localPath, expectedList.size(), actualList.size()));
        }
        for (int i = 0; i < expectedList.size(); i++) {
            String localPathList = localPath + "[" + i + "]";
            assertYamlEqualsInternal(localPathList, expectedList.get(i), actualList.get(i));
        }
    }


    private static String failureMessage(String message, String path, Object expected, Object actual){

        if(path == "") path = "<ROOT>";

        String fullMessage =  message;
        if(expected != null && actual != null){
            fullMessage += "\n\tExpected :\t%s\n" +
                    "\tActual :\t%s";
        }

        return String.format(fullMessage, path, expected, actual);

    }

    private static String failureMessage(String message, String path){
        return failureMessage(message, path, null,null);

    }
}
